# BS23 Flutter-Test-101_jubayer
## Basic Features
-In home page show a Lists
-list wil be sorted by a button
-List will be saved in local storage
-Loaded lists will be showen without internet
-All data is paginated
-offline data also show with pagination
-Every list of item  have a details page,there are indetails data as requirement
## Tech Features
-Used here HTTP for netwok call
-follow every page with abstraction layer
-Used Getx for state-management
-Sqflight used as local data
-follow abstraction layer for repository
-Central error handling and network call also implemented
-Used extension to allow us to add functionality to existing libraries and classes
-follow Getx folder structure
-Used Logprint for manage console print in debug and release mood.
-follow best repository pattern
-Use interface
-Used reusable functions
## Packages
-Getx
-HTTP
-Path_Provider
-intl
-flutter-toast
-sqflight
#Testing
-implement unit testing
### About Gitlab
-project uploaded in Gitlab
-two branches avaible
-main branch is updated
