// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.
import 'package:bs23_flutter_test_101_jubayer/app/common/utils/exports.dart';
import 'package:bs23_flutter_test_101_jubayer/app/enums/repository_sort_by.dart';
import 'package:bs23_flutter_test_101_jubayer/app/modules/home/controllers/home_controller.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:bs23_flutter_test_101_jubayer/main.dart' as app;



void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  test('date to string format extension test', () async {
    expect(Utils.dateStringToFormat("2015-03-06T22:54:58Z"), "03-06-15, 10:54 PM");
  });
  test('sort by star or update', () async {
    app.main();
    final HomeController homeController = Get.put(HomeController());

    homeController.repositorySortBy.value = RepositorySortBy.stars;
    expect(homeController.repositorySortBy.value, RepositorySortBy.stars);

    homeController.onSortByChange(RepositorySortBy.stars);
    expect(homeController.repositorySortBy.value, RepositorySortBy.stars);

    homeController.onSortByChange(RepositorySortBy.updated);
    expect(homeController.repositorySortBy.value, RepositorySortBy.updated);
  });
}
