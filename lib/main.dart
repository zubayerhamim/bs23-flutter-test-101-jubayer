import 'package:flutter/material.dart';
import 'app/common/utils/initializer.dart';
import 'bs23.dart';

void main() {
  Initializer.instance.init(() {
    runApp(const BS23());
  });
}

