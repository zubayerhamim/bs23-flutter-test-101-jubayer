extension DoubleToRealNumber on int {
  // 1500 - 1.5k
  // 10,000 - 10k
  // 100,000 - 100k
  String get compactShort {
    return this < 1000 ? toString() : "${(this / 1000).toStringAsFixed(1)}k";
  }
}
