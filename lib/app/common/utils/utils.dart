import 'package:bs23_flutter_test_101_jubayer/app/common/utils/exports.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';

class Utils {
  static void copyToClipBoard(String text) {
    if (text.isNotEmpty) {
      Clipboard.setData(ClipboardData(text: text));
      Fluttertoast.showToast(
          msg: "Copied",
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0,
        );
    }
  }

  static String dateStringToFormat(String date) {
    return DateFormat("MM-dd-yy, hh:mm a").format(DateTime.parse(date));
  }
}