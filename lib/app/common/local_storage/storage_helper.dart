import 'dart:convert';
import 'package:bs23_flutter_test_101_jubayer/app/common/utils/log_print.dart';
import 'package:bs23_flutter_test_101_jubayer/app/enums/repository_sort_by.dart';
import 'package:bs23_flutter_test_101_jubayer/app/modules/home/model/git_repositories.dart';
import 'package:sqflite/sqflite.dart';
import 'package:bs23_flutter_test_101_jubayer/app/common/local_storage/table/tables.dart';
import 'package:path/path.dart';
part 'local_storage.dart';

class StorageHelper {
  /// Save data
  static Future saveRepositories(List<GitRepository>repositories,RepositorySortBy sortBy) async {
    int mostStarRepositorySerial = await totalMostStarRepositories();
    int updatedRepositorySerial = await totalUpdatedRepositories();
    for (int i = 0; i < repositories.length; i++) {
      GitRepository repository = repositories[i];

      String query = '''
        SELECT * 
        FROM ${_StorageKeyWord.repositoriesTable}
        WHERE ${RepositoryTableFields.id} == ${repository.id}
      ''';
      var result = await _LocalStorage.instance.read(query);

      // if not data exist with this id then insert
      if(result.length == 0) {
        sortBy == RepositorySortBy.stars
            ? mostStarRepositorySerial++
            : updatedRepositorySerial++;

        String insertQuery = '''
        INSERT INTO ${_StorageKeyWord.repositoriesTable} (
          ${RepositoryTableFields.id},
          ${RepositoryTableFields.mostStarSerial},
          ${RepositoryTableFields.updatedSerial},
          ${RepositoryTableFields.repoDetails},
          ${RepositoryTableFields.ownerDetails}
        )
        VALUES (?, ?, ?, ?, ?)
        ''';

        int result = await _LocalStorage.instance.create(insertQuery, [
          repository.id,
          sortBy == RepositorySortBy.stars ? mostStarRepositorySerial : -1,
          sortBy == RepositorySortBy.updated ? updatedRepositorySerial : -1,
          jsonEncode(repository.toJson()),
          jsonEncode(repository.owner?.toJson())
        ]);

        if(result == -1) {
          LogPrint.print("Failed to insert. id: ${repository.id}");
        }

      }
      // update
      else {
        String column = sortBy == RepositorySortBy.stars
            ? RepositoryTableFields.mostStarSerial
            : RepositoryTableFields.updatedSerial;

        if (result.first[column] == -1) {
          int serial = sortBy == RepositorySortBy.stars
              ? mostStarRepositorySerial
              : updatedRepositorySerial;

          await updateRepositorySortBySerial(repository, sortBy, serial);
        }

      }
    }
  }

  static Future<int> totalMostStarRepositories()async {
    return await _countUniqueRow(RepositoryTableFields.mostStarSerial);
  }

  static Future<int> totalUpdatedRepositories() async {
    return await _countUniqueRow(RepositoryTableFields.updatedSerial);
  }

  static Future<List<GitRepository>> getRepositories(
      RepositorySortBy sortBy,
      int page, {
        int limit = 10,
      }) async {

    String column = sortBy == RepositorySortBy.stars
        ? RepositoryTableFields.mostStarSerial
        : RepositoryTableFields.updatedSerial;

    String query = '''
      SELECT * 
      FROM ${_StorageKeyWord.repositoriesTable} 
      WHERE $column != -1 AND $column > ${page * 10 - 10}
      ORDER BY $column ASC
      LIMIT $limit
    ''';

    var result = await _LocalStorage.instance.read(query);

    return _resultToObject(result);
  }

  /// read
  static Future<List<GitRepository>> getAllMostStarRepositories() async {
    return await _getAllRepositories(RepositorySortBy.stars).then((value) => value);
  }

  static Future<List<GitRepository>> getAllUpdatedRepositories() async {
    return await _getAllRepositories(RepositorySortBy.updated).then((value) => value);
  }

  /// update
  static Future updateRepositorySortBySerial(GitRepository repository, RepositorySortBy sortBy, int serial) async {
    String column = sortBy == RepositorySortBy.stars
        ? RepositoryTableFields.mostStarSerial
        : RepositoryTableFields.updatedSerial;

    String updateQuery = '''
          UPDATE ${_StorageKeyWord.repositoriesTable} 
          SET $column = $serial 
          WHERE ${RepositoryTableFields.id} = ${repository.id}
        ''';

    await _LocalStorage.instance.update(updateQuery);
  }

  /// helper
  static Future<int> _countUniqueRow(String column) async {
    String query = '''
      SELECT COUNT(*) 
      FROM ${_StorageKeyWord.repositoriesTable} 
      WHERE $column != -1
    ''';

    var count = await _LocalStorage.instance.read(query);
    return Sqflite.firstIntValue(count) ?? 0;
  }

  static Future<List<GitRepository>> _getAllRepositories(RepositorySortBy sortBy) async {
    String column = sortBy == RepositorySortBy.stars
        ? RepositoryTableFields.mostStarSerial
        : RepositoryTableFields.updatedSerial;

    String query = '''
      SELECT * 
      FROM ${_StorageKeyWord.repositoriesTable}
      WHERE $column != -1
      ORDER BY $column ASC
    ''';

    var result = await _LocalStorage.instance.read(query);

    return _resultToObject(result);
  }

  static List<GitRepository> _resultToObject(result) {
    List<GitRepository> repositories = [];

    for (var item in result) {
      GitRepository repository = GitRepository.fromJson(jsonDecode(item[RepositoryTableFields.repoDetails]));
      repository.owner = Owner.fromJson(jsonDecode(item[RepositoryTableFields.ownerDetails]));

      repositories.add(repository);
    }

    return repositories;
  }
}