part of 'storage_helper.dart';


class _StorageKeyWord {
  static String get repositoriesDb => 'repositoriesDb.db';

  static String get repositoriesTable => 'repositories';
}

class _LocalStorage {
  _LocalStorage._init();
  static final _LocalStorage instance = _LocalStorage._init();

  static Database? _database;

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath,filePath);
    return await openDatabase(path,version: 1,onCreate: _createDB);
  }

  Future _createDB(Database db, int version) async {
    await db.execute('''CREATE TABLE ${_StorageKeyWord.repositoriesTable}(
      ${RepositoryTableFields.id}'INTEGER PRIMARY KEY',
      ${RepositoryTableFields.mostStarSerial}'INTEGER NOT NULL',
      ${RepositoryTableFields.updatedSerial}'INTEGER NOT NULL',
      ${RepositoryTableFields.repoDetails}'TEXT NOT NULL',
      ${RepositoryTableFields.ownerDetails}'TEXT NOT NULL'
      )''');
  }

  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await _initDB(_StorageKeyWord.repositoriesDb);
    return _database!;
  }

  Future close()async {
    final db = await instance.database;
    db.close();
  }

  /// Create[create]database to[repositoriesDb]
  Future<int> create(String query, [List<Object?>? arguments]) async {
    final db = await instance.database;
    return await db.rawInsert(query, arguments);
  }

  /// read[read]database from[repositoriesDb]
  Future read(String query)async {
    final db = await instance.database;
    return await db.rawQuery(query);
  }

  /// Update[update]database from[repositoriesDb]
  Future update(String query)async {
    final db = await instance.database;
    return await db.rawQuery(query);
  }

}
