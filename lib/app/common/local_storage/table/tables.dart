class RepositoryTableFields {
  static const String id = 'id';
  static const String mostStarSerial = 'mostStarSerial';
  static const String updatedSerial = 'updatedSerial';
  static const String repoDetails = 'repoDetails';
  static const String ownerDetails = 'ownerDetails';
}