import 'package:bs23_flutter_test_101_jubayer/app/common/utils/exports.dart';
import 'package:bs23_flutter_test_101_jubayer/app/common/utils/utils.dart';

class CopyToClipBoardIcon extends StatelessWidget {
  final String? value;
  const CopyToClipBoardIcon({Key? key,required this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: value!.isNotEmpty,
      child: GestureDetector(
        onTap: () => Utils.copyToClipBoard(value!),
        child: const Icon(
          Icons.copy_rounded,
          size: 13,
          color: Colors.grey,
        ),
      ),
    );
  }
}