import 'dart:convert';

class GitRepositories {
  final int? totalCount;
  final bool? incompleteResults;
  final List<GitRepository>? gitRepository;

  GitRepositories({
    this.totalCount,
    this.incompleteResults,
    this.gitRepository,
  });

  factory GitRepositories.fromRawJson(String str) =>
      GitRepositories.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory GitRepositories.fromJson(Map<String, dynamic> json) =>
      GitRepositories(
        totalCount: json["total_count"],
        incompleteResults: json["incomplete_results"],
        gitRepository: json["items"] == null
            ? []
            : List<GitRepository>.from(json["items"]!.map((x) => GitRepository.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
    "total_count": totalCount,
    "incomplete_results": incompleteResults,
    "git_repository": gitRepository == null
        ? []
        : List<dynamic>.from(gitRepository!.map((x) => x.toJson())),
  };
}

class GitRepository {
  GitRepository({
    this.id,
    this.nodeId,
    this.name,
    this.fullName,
    this.private,
    this.owner,
    this.htmlUrl,
    this.description,
    this.fork,
    this.url,
    this.createdAt,
    this.updatedAt,
    this.pushedAt,
    this.homepage,
    this.size,
    this.stargazersCount,
    this.watchersCount,
    this.language,
    this.forksCount,
    this.openIssuesCount,
    this.topics,
    this.visibility,
    this.forks,
    this.openIssues,
    this.watchers,
    this.defaultBranch,
    this.score,
  });

  final int? id;
  final String? nodeId;
  final String? name;
  final String? fullName;
  final bool? private;
  Owner? owner;
  final String? htmlUrl;
  final String? description;
  final bool? fork;
  final String? url;
  final String? createdAt;
  final String? updatedAt;
  final String? pushedAt;
  final String? homepage;
  final int? size;
  final int? stargazersCount;
  final int? watchersCount;
  final String? language;
  final int? forksCount;
  final int? openIssuesCount;
  final List<String>? topics;
  final String? visibility;
  final int? forks;
  final int? openIssues;
  final int? watchers;
  final String? defaultBranch;
  final double? score;

  factory GitRepository.fromRawJson(String str) =>
      GitRepository.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory GitRepository.fromJson(Map<String, dynamic> json) => GitRepository(
    id: json["id"],
    nodeId: json["node_id"],
    name: json["name"],
    fullName: json["full_name"],
    private: json["private"],
    owner: json["owner"] == null ? null : Owner.fromJson(json["owner"]),
    htmlUrl: json["html_url"],
    description: json["description"],
    fork: json["fork"],
    url: json["url"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
    pushedAt: json["pushed_at"],
    homepage: json["homepage"],
    size: json["size"],
    stargazersCount: json["stargazers_count"],
    watchersCount: json["watchers_count"],
    language: json["language"],
    forksCount: json["forks_count"],
    openIssuesCount: json["open_issues_count"],
    topics: json["topics"] == null
        ? []
        : List<String>.from(json["topics"]!.map((x) => x)),
    visibility: json["visibility"],
    forks: json["forks"],
    openIssues: json["open_issues"],
    watchers: json["watchers"],
    defaultBranch: json["default_branch"],
    score: double.parse(json["score"].toString()),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "node_id": nodeId,
    "name": name,
    "full_name": fullName,
    "private": private,
    // "owner": jsonEncode(owner?.toJson()),
    "html_url": htmlUrl,
    "description": description,
    "fork": fork,
    "url": url,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "pushed_at": pushedAt,
    "homepage": homepage,
    "size": size,
    "stargazers_count": stargazersCount,
    "watchers_count": watchersCount,
    "language": language,
    "forks_count": forksCount,
    "open_issues_count": openIssuesCount,
    "topics": topics == null
        ? []
        : List<dynamic>.from(topics!.map((x) => x)),
    "visibility": visibility,
    "forks": forks,
    "open_issues": openIssues,
    "watchers": watchers,
    "default_branch": defaultBranch,
    "score": score,
  };
}

class Owner {
  Owner({
    this.login,
    this.id,
    this.avatarUrl,
    this.htmlUrl,
    this.type,
    this.siteAdmin,
  });

  final String? login;
  final int? id;
  final String? avatarUrl;
  final String? htmlUrl;
  final String? type;
  final bool? siteAdmin;

  factory Owner.fromRawJson(String str) => Owner.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Owner.fromJson(Map<String, dynamic> json) => Owner(
    login: json["login"],
    id: json["id"],
    avatarUrl: json["avatar_url"],
    htmlUrl: json["html_url"],
    type: json["type"],
    siteAdmin: json["site_admin"],
  );

  Map<String, dynamic> toJson() => {
    "login": login,
    "id": id,
    "avatar_url": avatarUrl,
    "html_url": htmlUrl,
    "type": type,
    "site_admin": siteAdmin,
  };
}
