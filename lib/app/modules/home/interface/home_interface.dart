import 'package:bs23_flutter_test_101_jubayer/app/enums/repository_sort_by.dart';

abstract class HomeInterface {
  void onItemTap(int index);
  void onSortByChange(RepositorySortBy repositorySortBy);
}
