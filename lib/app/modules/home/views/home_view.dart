import 'package:bs23_flutter_test_101_jubayer/app/common/extension/extension_number.dart';
import 'package:bs23_flutter_test_101_jubayer/app/enums/repository_sort_by.dart';
import 'package:bs23_flutter_test_101_jubayer/app/modules/home/model/git_repositories.dart';
import 'package:flutter/cupertino.dart';
import '../../../common/utils/exports.dart';
import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          actions: [_sortOptions],
          title: const Text('BS23_Jubayer'),
        ),
        body: Obx(() => !controller.firstLoading.value
            ? _mainBodyBuilder
            : const Center(child: CupertinoActivityIndicator())));
  }

  Widget get _mainBodyBuilder => ListView.builder(
      shrinkWrap: true,
      controller: controller.scrollController,
      itemCount: controller.repositories.length,
      itemBuilder: (context, index) {
        final GitRepository repository = controller.repositories[index];
        int counter = index +1;
        if(index == controller.repositories.length - 1) {
          return Column(
            children: [
              _repoItem(repository, index),
              const Center(child: CircularProgressIndicator()),
            ],
          );
        }
        return InkWell(
            onTap: () => controller.onItemTap(index),
            child: _repoItem(repository,counter));
      });

  Widget get _sortOptions => Obx(
        () => Container(
          padding: const EdgeInsets.all(5),
          margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 13),
          height: 30,
          width: 125,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5), color: Colors.white),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Icon(
                Icons.sort_outlined,
                color: Colors.black.withOpacity(0.5),
              ),
              const SizedBox(
                width: 5,
              ),
              DropdownButtonHideUnderline(
                child: DropdownButton<RepositorySortBy>(
                  value: controller.repositorySortBy.value,
                  items: <DropdownMenuItem<RepositorySortBy>>[
                    ...RepositorySortBy.values.map((e) {
                      return DropdownMenuItem(
                        alignment: Alignment.center,
                        value: e,
                        child: Text(e.name.capitalize!,textAlign: TextAlign.start,),
                      );
                    })
                  ],
                  onChanged: (value) => controller.onSortByChange(value!),
                ),
              ),
            ],
          ),
        ),
      );

  Widget _repoItem(GitRepository? repository, int counter) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 12, vertical: 10),
      padding: const EdgeInsets.all(12),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(child: Text(repository!.name!,maxLines: 2,style: const TextStyle(overflow: TextOverflow.ellipsis),)),
              Container(
                height: 30,
                width: 30,
                padding: const EdgeInsets.all(5),
                margin: const EdgeInsets.all(5),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black.withOpacity(0.2)),
                    shape: BoxShape.circle,
                  // borderRadius: BorderRadius.circular(25),
                ),
                child: Center(child: Text(counter.toString(),maxLines: 1,style: const TextStyle(fontSize: 10,overflow: TextOverflow.ellipsis),)),
              ),
            ],
          ),
          const SizedBox(height: 3,),
          Text(repository.language ?? "-"),
          const SizedBox(height: 3,),
          Text(repository.description ??"-",maxLines: 5,style:const TextStyle(overflow: TextOverflow.ellipsis),),
          const SizedBox(height: 5,),
          Row(
            children: [
              _totalActivitiesView(viewType: "Watch :",viewCount: repository.watchersCount!.compactShort,icon: Icons.remove_red_eye_outlined),
              const SizedBox(width: 8,),
              _totalActivitiesView(viewType: "Fork :",viewCount: repository.forksCount!.compactShort,icon: Icons.network_ping_rounded),
              const SizedBox(width: 8,),
              _totalActivitiesView(viewType: "Stars :",viewCount: repository.stargazersCount!.compactShort,icon: Icons.star_border),
            ],
          ),
        ],
      ),
    );
  }

  Widget _totalActivitiesView({required String viewType,String? viewCount,IconData? icon}) {
    return Expanded(
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 5,vertical: 5),
        decoration: BoxDecoration(
            border: Border.all(color: Colors.black.withOpacity(0.5)),
            borderRadius: BorderRadius.circular(5)

        ),
        child: Row(
          children: [
            const Spacer(),
            Icon(icon,size: 16,),
            const SizedBox(width: 3,),
            Text(viewType,style: const TextStyle(fontSize: 10),),
            const Spacer(),
            Container(
              padding: const EdgeInsets.all(4),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Colors.grey
              ),
              child: Text("$viewCount",style: const TextStyle(fontSize: 9,fontWeight: FontWeight.w600),),
            ),
            const Spacer(),
          ],
        ),
      ),
    );
  }
}



