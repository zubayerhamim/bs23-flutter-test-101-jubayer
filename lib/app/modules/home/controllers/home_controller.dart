import 'package:bs23_flutter_test_101_jubayer/app/common/local_storage/storage_helper.dart';
import 'package:bs23_flutter_test_101_jubayer/app/enums/repository_sort_by.dart';
import 'package:bs23_flutter_test_101_jubayer/app/modules/home/model/git_repositories.dart';
import 'package:bs23_flutter_test_101_jubayer/app/repository/network_helper_impl.dart';
import 'package:bs23_flutter_test_101_jubayer/app/routes/app_pages.dart';
import '../../../common/utils/exports.dart';
import '../../../repository/network_helper.dart';
import '../interface/home_interface.dart';

class HomeController extends GetxController implements HomeInterface {

  final NetworkHelper _networkHelper = Get.find();

  RxList<GitRepository> repositories = <GitRepository>[].obs;

  /// [firstLoading] will TRUE when call [onInit] or change [repositorySortBy]
  RxBool firstLoading = true.obs;

  /// [loadMoreLoading] will true when last item is visible
  RxBool loadMoreLoading = false.obs;

  /// [_loadMoreFromServer] will Ture when fetch data form server and [firstLoading] will false
  /// basically [_loadMoreFromServer] used for prevent multiple request when [scrollController] reached to max
  bool _loadMoreFromServer = false;

  /// page
  int _page = 1;

  /// used [scrollController] for pagination
  late ScrollController scrollController;

  /// [items] is used for passing data to next route
  GitRepository? items;

  /// sort repository
  Rx<RepositorySortBy> repositorySortBy = RepositorySortBy.stars.obs;

  /// getter
  bool get isRepositorySortByStar => repositorySortBy.value == RepositorySortBy.stars;
  bool get isRepositorySortByUpdated => !isRepositorySortByStar;

  @override
  void onInit() {
    scrollController = ScrollController()..addListener(_scrollListener);
    _getInitValue();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  Future<void> _getInitValue() async {
    int totalRepo = await StorageHelper.totalMostStarRepositories();
    if(isRepositorySortByUpdated) totalRepo = await StorageHelper.totalUpdatedRepositories();

    if(totalRepo == 0 || totalRepo == repositories.length) {
      _fetchGitRepositoryFromNetwork();
    } else {
      if(loadMoreLoading.value) return;
      if(!firstLoading.value) loadMoreLoading.value = true;
      List<GitRepository> data =  await StorageHelper.getRepositories(
        repositorySortBy.value,
        _page,
      );

      repositories..addAll(data)..refresh();
      _page++;

      loadMoreLoading.value = false;
      firstLoading.value = false;
    }
  }

  Future<void> _fetchGitRepositoryFromNetwork()async {
    if(_loadMoreFromServer)return;
    LogPrint.print("Fetch form ONLINE");
    _loadMoreFromServer = true;
    try {
      var response = await _networkHelper.getSearchInfo(page: _page, perPageItem: 10, sortBy: repositorySortBy.value);
      if (response.gitRepository!.isNotEmpty) {
        _updateNewRepositories(response.gitRepository!);
      }
    } catch (e) {
      _loadMoreFromServer = false;
    }
  }

  Future<void> _updateNewRepositories(List<GitRepository> list) async {
    await StorageHelper.saveRepositories(list, repositorySortBy.value);

    loadMoreLoading.value = false;
    firstLoading.value = false;
    _loadMoreFromServer = false;
    _page++;

    repositories..addAll(list)..refresh();
  }

  void _scrollListener() {
    if (scrollController.position.extentAfter < 10) {
      _getInitValue();
    }
  }


  @override
  void onItemTap(int index) {
    Get.toNamed(Routes.repositoryDetails,
        arguments: {items : repositories[index]});
  }

  @override
  void onSortByChange(RepositorySortBy repositorySortBy) {
    if (this.repositorySortBy.value != repositorySortBy) {
      this.repositorySortBy.value = repositorySortBy;
      _onRefresh();
    }
  }

  void _onRefresh() {
    firstLoading.value = true;
    loadMoreLoading.value = false;
    _page = 1;

    repositories..clear()..refresh();

    _getInitValue();

  }

}
