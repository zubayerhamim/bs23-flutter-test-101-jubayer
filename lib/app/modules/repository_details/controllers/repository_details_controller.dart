import 'package:bs23_flutter_test_101_jubayer/app/modules/home/controllers/home_controller.dart';
import 'package:bs23_flutter_test_101_jubayer/app/modules/home/model/git_repositories.dart';
import 'package:get/get.dart';

class RepositoryDetailsController extends GetxController {
 /// [HomeController] is only used for avoiding extra class or variable,
 final HomeController homeController = Get.find();

  /// selected item store here[gitRepositoryItem]
  late GitRepository gitRepositoryItem;

  @override
  void onInit() {
    gitRepositoryItem = Get.arguments[homeController.items];
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
