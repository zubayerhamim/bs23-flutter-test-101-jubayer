import 'package:bs23_flutter_test_101_jubayer/app/common/utils/exports.dart';
import 'package:bs23_flutter_test_101_jubayer/app/common/utils/utils.dart';
import 'package:bs23_flutter_test_101_jubayer/app/common/widgets/copy_to_clip_board.dart';
import 'package:bs23_flutter_test_101_jubayer/app/modules/home/model/git_repositories.dart';
import 'package:flutter/cupertino.dart';

class PillView extends StatelessWidget {
 final GitRepository gitRepositoryItem;
 const PillView({Key? key,required this.gitRepositoryItem}) : super(key: key);

   @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin:const EdgeInsets.all(10),
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          border: Border.all(),

        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(gitRepositoryItem.fullName ?? "No name found",maxLines: 2,style:const TextStyle(fontWeight: FontWeight.w600,overflow: TextOverflow.ellipsis),),
                    const SizedBox(height: 5,),
                    _repoStatus,
                    const SizedBox(height: 5,),
                    gitRepositoryItem.language == null ? const SizedBox(): _language,
                    const SizedBox(height: 5,),
                    _branch,
                  ],
                ),
              ),
              _imageView(imgUrl: gitRepositoryItem.owner!.avatarUrl!),
            ],
          ),
            const SizedBox(height: 5,),
            _url,
            const SizedBox(height: 5,),
            _updatedDate,
            const SizedBox(height: 10,),
            _description,
            const SizedBox(height: 10,),
            gitRepositoryItem.topics!.isNotEmpty
                ? _allTopics
                : const SizedBox(),
          ],
        ),
      ),
    );
  }

  Widget get _language => Row(children: [
    const Text("Language : "),
    Text(gitRepositoryItem.language ?? "",style: const TextStyle(fontWeight: FontWeight.w600),),
  ],);

  Widget get _repoStatus => Row(
    children: [
      const Text("Repo Status : "),
      gitRepositoryItem.private == true
          ? const Text("Private",style: TextStyle(fontWeight: FontWeight.w600),)
          : const Text("Public",style: TextStyle(fontWeight: FontWeight.w600),),
      const SizedBox(width: 3,),
      gitRepositoryItem.private == true ? const Icon(Icons.lock_outline,size:14,): const Icon(Icons.lock_open_outlined,size:14,)
    ],
  );

  Widget get _branch => Row(
    children: [
      const Text("Default Branch : "),
      Text(gitRepositoryItem.defaultBranch ?? "",style: const TextStyle(fontWeight: FontWeight.w600),),
    ],
  );

  Widget _imageView({String? imgUrl}) => Container(
    margin:const EdgeInsets.all(3),
    height:80,
    width: 80,
    decoration: BoxDecoration(
        border: Border.all(color: Colors.green,),
        shape: BoxShape.circle
    ),
    child: ClipRRect(
      borderRadius: BorderRadius.circular(40),
      child: Center(child: Image.network(imgUrl!,
          errorBuilder: (context, error, stackTrace) {
            return Center(child: ClipRRect(
                borderRadius: BorderRadius.circular(40),
                child: Image.asset("assets/images/no-internet.jpg"))); //do something
          },
          loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent? loadingProgress) {
            if (loadingProgress == null) return child;
            return const CupertinoActivityIndicator();
          },
          )),
    ),
  );

  Widget get _url => Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text("Url : "),
          Expanded(
              child: Text(
            gitRepositoryItem.url ?? "",
            style: const TextStyle(color: Colors.blue,
              fontWeight: FontWeight.w500,
              decoration: TextDecoration.underline,
            ),
          )),
           CopyToClipBoardIcon( value: '${gitRepositoryItem.url}',),
        ],
      );

  Widget get _updatedDate => Row(
   children: [
     const Text("Updated : "),
     Text(Utils.dateStringToFormat(gitRepositoryItem.updatedAt ?? DateTime.now().toString()),style:const TextStyle(fontWeight: FontWeight.w600),),
   ],
 );

  Widget get _description => Wrap(
   children: [
     const Text("Description : ",style: TextStyle(fontWeight: FontWeight.w600),),
     Text(gitRepositoryItem.description ?? ""),
   ],
 );

  Widget get _allTopics =>  Wrap(
    spacing: 6,
    runSpacing: 5,
    children: [
      const Text("All Topics : ",style: TextStyle(fontWeight: FontWeight.w600),),
      ...gitRepositoryItem.topics!.map((e) => Container(
        padding: const EdgeInsets.all(5),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          border: Border.all(color: Colors.black.withOpacity(0.3)),
        ),
        child: Text(e),
      ) ),
    ],
  );
}
