import 'package:bs23_flutter_test_101_jubayer/app/modules/repository_details/views/widgets/pill_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controllers/repository_details_controller.dart';

class RepositoryDetailsView extends GetView<RepositoryDetailsController> {
  const RepositoryDetailsView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(controller.gitRepositoryItem.name ?? "Item Details"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            PillView(gitRepositoryItem: controller.gitRepositoryItem),
          ],
        ),
      ),
    );
  }
}
