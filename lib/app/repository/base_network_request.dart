import 'dart:async';
import 'dart:io';
import 'package:bs23_flutter_test_101_jubayer/app/common/utils/exports.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get_connect/http/src/exceptions/exceptions.dart';
import 'package:http/http.dart' as http;

class BaseNetworkRequest {

  /// get
  static Future getRequest(String url) async {
    try {
      http.Response response = await http.get(Uri.parse(url), headers: _getHeaders);
      return _responseModifier(response);
    } on TimeoutException {
      if (kDebugMode) {
        LogPrint.print("Request Timeout");
      }
    } on SocketException {
      if (kDebugMode) {
        LogPrint.print("Check Connection");
      } return null;
    } on FormatException {
      LogPrint.print("Invalid Format");
    } catch (e, s) {
      if (kDebugMode) {
        print(e.toString());
        print(s);
      }
      return null;
    }
  }

  /// post
  static void postRequest() {}

  /// put
  static void putRequest() {}

  /// delete
  static void deleteRequest() {}

  static get _getHeaders {
    String token = "";

    if (token.isEmpty) return null;

    return {
      HttpHeaders.authorizationHeader: 'Token $token',
      HttpHeaders.contentTypeHeader: 'application/json',
    };
  }

  static dynamic _responseModifier(http.Response response) {
    switch(response.statusCode){
      case 200:
        return response;
      case 400:
      case 500:
      default:
        throw UnexpectedFormat("Http status code error -> ${response.statusCode}");
    }
  }

}