import 'dart:convert';
import 'dart:io';
import 'package:bs23_flutter_test_101_jubayer/app/enums/repository_sort_by.dart';
import 'package:bs23_flutter_test_101_jubayer/app/modules/home/model/git_repositories.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import '../common/utils/log_print.dart';
import 'base_network_request.dart';
import 'network_helper.dart';
import 'network_urls.dart';

class NetworkHelperImplement implements NetworkHelper {

  dynamic _convert<T>(String response,Function(Map<String, dynamic>) base,) {
    try {
      return base(json.decode(response)) as T;
    } catch (e, s) {
      if (kDebugMode) {
        LogPrint.print("RESPONSE TO MODEL CONVERT ERROR");
        LogPrint.print(e.toString());
      }
    }
  }

  @override
  Future<GitRepositories> getSearchInfo({int? page,int? perPageItem, RepositorySortBy? sortBy}) async {
    String baseUrl = NetworkUrls.base;
    String urlSearchKey = "Flutter";
    String url = "${baseUrl}q=$urlSearchKey";

    if (page != null) url += "&page=$page";

    if (perPageItem != null) url += "&per_page=$perPageItem";

    if (sortBy != null) url += "&sort=${sortBy.name}";
    http.Response response = await BaseNetworkRequest.getRequest(url);
    return _convert<GitRepositories>(response.body, GitRepositories.fromJson);
  }
}
