import 'package:bs23_flutter_test_101_jubayer/app/enums/repository_sort_by.dart';
import 'package:bs23_flutter_test_101_jubayer/app/modules/home/model/git_repositories.dart';

abstract class NetworkHelper {
  Future<GitRepositories> getSearchInfo({int? page,int? perPageItem,RepositorySortBy? sortBy,});
}
