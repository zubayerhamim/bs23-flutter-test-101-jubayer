import 'app/common/app_info/app_info.dart';
import 'app/common/utils/exports.dart';
import 'app/common/utils/initializer.dart';
import 'app/routes/app_pages.dart';

class BS23 extends StatelessWidget {
  const BS23({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: AppInfo.appName,
      initialRoute: AppPages.initial,
      initialBinding: InitialBindings(),
      getPages: AppPages.routes,
      debugShowCheckedModeBanner: false,
      defaultTransition: Transition.rightToLeft,
    );
  }
}
